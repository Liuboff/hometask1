let spoilerTitles = document.querySelectorAll('.mySpoiler__title');

spoilerTitles.forEach(title => {
    title.addEventListener('click', (e) => {
        e.target.classList.toggle("active");
        e.target.nextElementSibling.classList.toggle("active__text");
    })
});